package academy.kata.test;

import academy.kata.test.helpers.RomeDigitsHelper;

public class Args {
    int arg1;
    int arg2;
    String operator;
    boolean isArabic = true;

    public Args(String input) {
        String[] args = input.split(" ");
        if (args.length != 3) {
            throw new IllegalArgumentException("Arguments count must be 3");
        }
        operator = args[1];

        RomeDigitsHelper helper = new RomeDigitsHelper();

        if (helper.isRome(args[0])) {
            arg1 = helper.parseRome(args[0]);
            isArabic = false;
        } else {
            arg1 = Integer.parseInt(args[0]);
        }

        if (helper.isRome(args[2])) {
            if (isArabic) {
                throw new IllegalArgumentException("Both arguments must be rome or arabic digits");
            }
            arg2 = helper.parseRome(args[2]);
        } else {
            if (!isArabic) {
                throw new IllegalArgumentException("Both arguments must be rome or arabic digits");
            }
            arg2 = Integer.parseInt(args[2]);
        }

        if (arg1 < 1 || arg1 > 10 || arg2 < 1 || arg2 > 10) {
            throw new IllegalArgumentException("Arguments must be in range 1-10");
        }
    }

}
