package academy.kata.test.helpers;

public class RomeDigitsHelper {
    public int parseRome(String arg) {
        int result = 0;
        while (!arg.isEmpty()) {
            if (arg.contains("XC")) {
                result += 90;
                arg = arg.replaceFirst("XC", "");
            } else if (arg.contains("XL")) {
                result += 40;
                arg = arg.replaceFirst("XL", "");
            } else if (arg.contains("IX")) {
                result += 9;
                arg = arg.replaceFirst("IX", "");
            } else if (arg.contains("IV")) {
                result += 4;
                arg = arg.replaceFirst("IV", "");
            } else if (arg.contains("C")) {
                result += 100;
                arg = arg.replaceFirst("C", "");
            } else if (arg.contains("L")) {
                result += 50;
                arg = arg.replaceFirst("L", "");
            } else if (arg.contains("X")) {
                result += 10;
                arg = arg.replaceFirst("X", "");
            } else if (arg.contains("V")) {
                result += 5;
                arg = arg.replaceFirst("V", "");
            } else if (arg.contains("I")) {
                result += 1;
                arg = arg.replaceFirst("I", "");
            }
        }

        return result;
    }

    public boolean isRome(String arg) {
        return "IVXLC".indexOf(arg.charAt(0)) > -1;
    }

    public String arabicToRome(int arabic) {
        StringBuilder romeDigit = new StringBuilder();
        while (arabic > 0) {
            if (arabic >= 10) {
                romeDigit.append("X");
                arabic -= 10;
            } else if (arabic >= 9) {
                romeDigit.append("IX");
                arabic -= 9;
            } else if (arabic >= 5) {
                romeDigit.append("V");
                arabic -= 5;
            } else if (arabic >= 4) {
                romeDigit.append("IV");
                arabic -= 4;
            } else {
                romeDigit.append("I");
                arabic -= 1;
            }
        }
        return romeDigit.toString();
    }
}
