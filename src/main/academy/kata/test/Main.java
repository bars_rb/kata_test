package academy.kata.test;

import academy.kata.test.helpers.RomeDigitsHelper;

public class Main {
    public static String calc(String input) {
        Args args = new Args(input);

        int result = calculate(args);
        if (args.isArabic) {
            return String.valueOf(result);
        } else {
            return new RomeDigitsHelper().arabicToRome(result);
        }
    }

    private static int calculate(Args args) {
        int result = 0;
        if (args.operator.equals("+")) {
            result = args.arg1 + args.arg2;
        }
        if (args.operator.equals("-")) {
            result = args.arg1 - args.arg2;
        }
        if (args.operator.equals("*")) {
            result = args.arg1 * args.arg2;
        }
        if (args.operator.equals("/")) {
            result = args.arg1 / args.arg2;
        }
        if (!args.isArabic && result < 1) {
            throw new ArithmeticException("In rome digits result mus be more then 1");
        }
        return result;
    }
}


