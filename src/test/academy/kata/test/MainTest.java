package academy.kata.test;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class MainTest {
    @Test
    public void calcArabicOnePlusTwoIsThree() {
        assertEquals("3", Main.calc("1 + 2"));
    }

    @Test
    public void calcRomeSixDivideThreeIsTwo() {
        assertEquals("II", Main.calc("VI / III"));
    }

    @Test(expected = ArithmeticException.class)
    public void calcRomeOneMinusTwoTrowsException() {
        Main.calc("I - II");
    }

    @Test(expected = IllegalArgumentException.class)
    public void calcRomeAndArabicTrowsException() {
        Main.calc("I + 1");
    }

    @Test(expected = IllegalArgumentException.class)
    public void calcMoreThanOneOperator() {
        Main.calc("1 + 2 + 3");
    }

    @Test(expected = IllegalArgumentException.class)
    public void calcArgumentMoreThen10() {
        Main.calc("XXX + I");
    }

    @Test
    public void calcArabicFiveMinusSixIsMinusOne() {
        assertEquals("-1", Main.calc("5 - 6"));
    }

    @Test
    public void calcArabicFiveMinusFiveIsZero() {
        assertEquals("0", Main.calc("5 - 5"));
    }

}